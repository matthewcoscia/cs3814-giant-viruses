import csv
import sys
import itertools

def main():

    if len(sys.argv) != 2:
        print("Error: You must include the range of the search. Try not to exceed the length of the tsv file.")
        return

    with open("vog_trimmed.tsv") as fd:
        rd = csv.reader(fd, delimiter="\t")
        n = 0
        #count = 0
        first = True
        genome = {}
        for row in itertools.islice(rd, 0, int(sys.argv[1])):
            if first:
                first = False
                genome = row
                continue

            sum = 0.0
            max = 0.0
            max_index = 0
            for i in range(1, len(row)): 
                sum += float(row[i])
                if float(row[i]) > max:
                    max = float(row[i])
                    max_index = i
            if sum > 100:
                n += 1
                print('{}: {}\tMax: {}\tin\t{}'.format(row[0], sum, max, genome[max_index]))
                #print(count)
            #count += 1
        print("There are {} VOGS that appear more than 100 times".format(n))

if __name__ == "__main__":
    main()