import csv
import sys
import itertools

def main():
    tsv = open("vog_annot.tsv")
    tr = open("tr.txt")

    matrix = tsv.readlines()
    offsets = tr.readlines()

    print(matrix[0].strip("\n"))
    for off in offsets:
        print(matrix[int(off.strip("\n"))].strip("\n"))

if __name__ == "__main__":
    main()