This output is from running matrix_analysis.py on the original vog_annot.tsv file.

VOG00004: 657.0,	Max: 5.0
VOG00006: 135.0,	Max: 12.0
VOG00007: 460.0,	Max: 3.0
VOG00011: 579.0,	Max: 7.0
VOG00012: 718.0,	Max: 18.0
VOG00018: 186.0,	Max: 4.0
VOG00024: 123.0,	Max: 6.0
VOG00034: 355.0,	Max: 2.0
VOG00035: 1207.0,	Max: 3.0
VOG00037: 725.0,	Max: 3.0
VOG00038: 140.0,	Max: 13.0
VOG00040: 327.0,	Max: 3.0
VOG00046: 571.0,	Max: 3.0
VOG00051: 542.0,	Max: 3.0
VOG00052: 129.0,	Max: 12.0
VOG00055: 408.0,	Max: 2.0
VOG00058: 305.0,	Max: 2.0
VOG00059: 375.0,	Max: 2.0
VOG00060: 747.0,	Max: 2.0
VOG00061: 143.0,	Max: 9.0
VOG00064: 141.0,	Max: 3.0
VOG00065: 114.0,	Max: 3.0
VOG00066: 778.0,	Max: 5.0
VOG00067: 166.0,	Max: 3.0
VOG00071: 677.0,	Max: 2.0
VOG00072: 222.0,	Max: 3.0
VOG00073: 198.0,	Max: 2.0
VOG00074: 237.0,	Max: 2.0
VOG00075: 380.0,	Max: 2.0
VOG00082: 326.0,	Max: 4.0
VOG00083: 509.0,	Max: 2.0
VOG00084: 223.0,	Max: 1.0
VOG00088: 284.0,	Max: 2.0
VOG00095: 165.0,	Max: 2.0
VOG00097: 123.0,	Max: 3.0
VOG00098: 187.0,	Max: 1.0
VOG00103: 748.0,	Max: 3.0
VOG00106: 240.0,	Max: 1.0
VOG00109: 655.0,	Max: 1.0
VOG00114: 133.0,	Max: 1.0
VOG00119: 140.0,	Max: 2.0
VOG00120: 164.0,	Max: 1.0
VOG00121: 118.0,	Max: 2.0
VOG00126: 323.0,	Max: 6.0
VOG00127: 217.0,	Max: 4.0
VOG00128: 119.0,	Max: 1.0
VOG00130: 190.0,	Max: 3.0
VOG00132: 190.0,	Max: 2.0
VOG00135: 144.0,	Max: 3.0
VOG00136: 120.0,	Max: 7.0
VOG00139: 105.0,	Max: 7.0
VOG00147: 236.0,	Max: 2.0
VOG00150: 103.0,	Max: 3.0
VOG00154: 116.0,	Max: 1.0
VOG00156: 115.0,	Max: 3.0
VOG00159: 149.0,	Max: 3.0
VOG00160: 119.0,	Max: 2.0
VOG00163: 104.0,	Max: 3.0
VOG00169: 200.0,	Max: 5.0
VOG00180: 312.0,	Max: 1.0
VOG00181: 300.0,	Max: 1.0
VOG00182: 102.0,	Max: 1.0
VOG00183: 663.0,	Max: 2.0
VOG00185: 143.0,	Max: 5.0
VOG00186: 162.0,	Max: 4.0
VOG00187: 159.0,	Max: 2.0
VOG00188: 272.0,	Max: 4.0
VOG00190: 255.0,	Max: 2.0
VOG00201: 201.0,	Max: 4.0
VOG00202: 185.0,	Max: 4.0
VOG00205: 213.0,	Max: 1.0
VOG00222: 154.0,	Max: 1.0
VOG00233: 224.0,	Max: 2.0
VOG00234: 174.0,	Max: 2.0
VOG00237: 235.0,	Max: 3.0
VOG00240: 130.0,	Max: 1.0
VOG00245: 158.0,	Max: 4.0
VOG00246: 298.0,	Max: 2.0
VOG00261: 104.0,	Max: 1.0
VOG00271: 188.0,	Max: 3.0
VOG00278: 470.0,	Max: 4.0
VOG00281: 493.0,	Max: 2.0
VOG00288: 561.0,	Max: 2.0
VOG00291: 108.0,	Max: 1.0
VOG00292: 342.0,	Max: 2.0
VOG00296: 129.0,	Max: 1.0
VOG00297: 172.0,	Max: 2.0
VOG00298: 348.0,	Max: 2.0
VOG00301: 136.0,	Max: 2.0
VOG00302: 629.0,	Max: 2.0
VOG00309: 204.0,	Max: 1.0
VOG00318: 282.0,	Max: 2.0
VOG00319: 194.0,	Max: 2.0
VOG00327: 101.0,	Max: 4.0
VOG00333: 107.0,	Max: 1.0
VOG00338: 373.0,	Max: 3.0
VOG00344: 176.0,	Max: 3.0
VOG00350: 207.0,	Max: 1.0
VOG00365: 351.0,	Max: 2.0
VOG00371: 624.0,	Max: 3.0
VOG00373: 408.0,	Max: 3.0
VOG00375: 132.0,	Max: 2.0
VOG00377: 278.0,	Max: 2.0
VOG00383: 126.0,	Max: 3.0
VOG00384: 105.0,	Max: 2.0
VOG00385: 131.0,	Max: 2.0
VOG00386: 162.0,	Max: 1.0
VOG00387: 236.0,	Max: 1.0
VOG00395: 160.0,	Max: 3.0
VOG00399: 126.0,	Max: 2.0
VOG00402: 329.0,	Max: 2.0
VOG00407: 130.0,	Max: 1.0
VOG00408: 371.0,	Max: 2.0
VOG00417: 143.0,	Max: 5.0
VOG00418: 215.0,	Max: 1.0
VOG00427: 114.0,	Max: 1.0
VOG00428: 171.0,	Max: 2.0
VOG00430: 243.0,	Max: 3.0
VOG00463: 670.0,	Max: 3.0
VOG00476: 351.0,	Max: 2.0
VOG00479: 132.0,	Max: 4.0
VOG00483: 170.0,	Max: 2.0
VOG00494: 229.0,	Max: 1.0
VOG00496: 148.0,	Max: 2.0
VOG00498: 155.0,	Max: 2.0
VOG00504: 161.0,	Max: 3.0
VOG00505: 260.0,	Max: 2.0
VOG00507: 195.0,	Max: 2.0
VOG00512: 107.0,	Max: 1.0
VOG00525: 251.0,	Max: 1.0
VOG00533: 157.0,	Max: 1.0
VOG00537: 246.0,	Max: 1.0
VOG00540: 108.0,	Max: 1.0
VOG00543: 129.0,	Max: 1.0
VOG00546: 123.0,	Max: 2.0
VOG00565: 139.0,	Max: 2.0
VOG00568: 133.0,	Max: 1.0
VOG00576: 270.0,	Max: 2.0
VOG00579: 101.0,	Max: 2.0
VOG00597: 153.0,	Max: 2.0
VOG00607: 135.0,	Max: 9.0
VOG00640: 575.0,	Max: 3.0
VOG00647: 345.0,	Max: 2.0
VOG00652: 129.0,	Max: 2.0
VOG00653: 531.0,	Max: 2.0
VOG00658: 262.0,	Max: 1.0
VOG00659: 203.0,	Max: 2.0
VOG00661: 163.0,	Max: 2.0
VOG00665: 439.0,	Max: 2.0
VOG00666: 300.0,	Max: 3.0
VOG00667: 438.0,	Max: 2.0
VOG00670: 123.0,	Max: 1.0
VOG00672: 239.0,	Max: 2.0
VOG00673: 314.0,	Max: 1.0
VOG00676: 259.0,	Max: 3.0
VOG00677: 287.0,	Max: 2.0
VOG00678: 127.0,	Max: 1.0
VOG00682: 198.0,	Max: 2.0
VOG00721: 215.0,	Max: 2.0
VOG00722: 196.0,	Max: 1.0
VOG00724: 102.0,	Max: 2.0
VOG00731: 218.0,	Max: 2.0
VOG00740: 169.0,	Max: 2.0
VOG00745: 206.0,	Max: 1.0
VOG00749: 192.0,	Max: 1.0
VOG00752: 107.0,	Max: 2.0
VOG00755: 693.0,	Max: 3.0
VOG00758: 269.0,	Max: 2.0
VOG00759: 215.0,	Max: 2.0
VOG00760: 133.0,	Max: 2.0
VOG00776: 135.0,	Max: 2.0
VOG00792: 124.0,	Max: 1.0
VOG00802: 160.0,	Max: 3.0
VOG00808: 118.0,	Max: 2.0
VOG00828: 109.0,	Max: 1.0
VOG00836: 101.0,	Max: 4.0
VOG00841: 104.0,	Max: 2.0
VOG00850: 121.0,	Max: 4.0
VOG00932: 142.0,	Max: 2.0
VOG00934: 352.0,	Max: 3.0
VOG00937: 137.0,	Max: 2.0
VOG00938: 187.0,	Max: 2.0
VOG00940: 149.0,	Max: 1.0
VOG00941: 260.0,	Max: 2.0
VOG00950: 192.0,	Max: 3.0
VOG00954: 430.0,	Max: 2.0
VOG00955: 198.0,	Max: 3.0
VOG00957: 150.0,	Max: 3.0
VOG00959: 194.0,	Max: 2.0
VOG00962: 120.0,	Max: 2.0
VOG00969: 124.0,	Max: 1.0
VOG00978: 1164.0,	Max: 2.0
VOG00979: 472.0,	Max: 2.0
VOG00980: 366.0,	Max: 2.0
VOG00981: 152.0,	Max: 2.0
VOG00985: 164.0,	Max: 1.0
VOG00987: 102.0,	Max: 1.0
VOG00999: 107.0,	Max: 2.0
VOG01008: 124.0,	Max: 1.0
VOG01018: 132.0,	Max: 2.0
VOG01021: 184.0,	Max: 2.0
VOG01032: 219.0,	Max: 2.0
VOG01038: 151.0,	Max: 3.0
VOG01047: 339.0,	Max: 2.0
VOG01076: 101.0,	Max: 1.0
VOG01079: 118.0,	Max: 2.0
VOG01083: 159.0,	Max: 2.0
VOG01087: 105.0,	Max: 1.0
VOG01097: 104.0,	Max: 1.0
VOG01100: 248.0,	Max: 2.0
VOG01103: 121.0,	Max: 1.0
VOG01132: 104.0,	Max: 1.0
VOG01137: 287.0,	Max: 2.0
VOG01138: 249.0,	Max: 1.0
VOG01142: 106.0,	Max: 1.0
VOG01146: 162.0,	Max: 2.0
VOG01160: 122.0,	Max: 2.0
VOG01171: 141.0,	Max: 1.0
VOG01182: 124.0,	Max: 2.0
VOG01197: 217.0,	Max: 4.0
VOG01215: 114.0,	Max: 1.0
VOG01221: 105.0,	Max: 2.0
VOG01265: 186.0,	Max: 1.0
VOG01281: 164.0,	Max: 1.0
VOG01312: 146.0,	Max: 1.0
VOG01362: 101.0,	Max: 2.0
VOG01365: 303.0,	Max: 2.0
VOG01389: 220.0,	Max: 2.0
VOG01391: 155.0,	Max: 1.0
VOG01396: 628.0,	Max: 3.0
VOG01403: 170.0,	Max: 1.0
VOG01404: 206.0,	Max: 2.0
VOG01405: 239.0,	Max: 3.0
VOG01415: 531.0,	Max: 3.0
VOG01420: 249.0,	Max: 2.0
VOG01424: 103.0,	Max: 2.0
VOG01431: 180.0,	Max: 2.0
VOG01442: 152.0,	Max: 2.0
VOG01444: 174.0,	Max: 1.0
VOG01445: 215.0,	Max: 1.0
VOG01447: 231.0,	Max: 2.0
VOG01464: 102.0,	Max: 2.0
VOG01472: 267.0,	Max: 3.0
VOG01473: 362.0,	Max: 2.0
VOG01474: 185.0,	Max: 2.0
VOG01476: 184.0,	Max: 1.0
VOG01478: 371.0,	Max: 2.0
VOG01481: 213.0,	Max: 2.0
VOG01488: 294.0,	Max: 2.0
VOG01503: 112.0,	Max: 1.0
VOG01514: 153.0,	Max: 2.0
VOG01517: 140.0,	Max: 1.0
VOG01524: 196.0,	Max: 2.0
VOG01526: 141.0,	Max: 2.0
VOG01534: 202.0,	Max: 2.0
VOG01559: 163.0,	Max: 1.0
VOG01565: 110.0,	Max: 2.0
VOG01571: 132.0,	Max: 2.0
VOG01584: 196.0,	Max: 2.0
VOG01590: 208.0,	Max: 3.0
VOG01593: 195.0,	Max: 1.0
VOG01603: 214.0,	Max: 2.0
VOG01604: 121.0,	Max: 2.0
VOG01647: 138.0,	Max: 1.0
VOG01653: 112.0,	Max: 2.0
VOG01668: 110.0,	Max: 1.0
VOG01697: 136.0,	Max: 4.0
VOG01704: 123.0,	Max: 2.0
VOG01706: 148.0,	Max: 2.0
VOG01720: 233.0,	Max: 2.0
VOG01751: 470.0,	Max: 4.0
VOG01763: 123.0,	Max: 1.0
VOG01770: 220.0,	Max: 1.0
VOG01810: 120.0,	Max: 1.0
VOG01820: 105.0,	Max: 1.0
VOG01865: 168.0,	Max: 2.0
VOG01887: 136.0,	Max: 2.0
VOG01966: 177.0,	Max: 3.0
VOG02029: 122.0,	Max: 1.0
VOG02039: 105.0,	Max: 2.0
VOG02313: 146.0,	Max: 1.0
VOG02482: 394.0,	Max: 2.0
VOG02502: 103.0,	Max: 2.0
VOG02509: 444.0,	Max: 1.0
VOG02514: 124.0,	Max: 2.0
VOG02522: 184.0,	Max: 2.0
VOG02524: 231.0,	Max: 2.0
VOG02539: 140.0,	Max: 2.0
VOG02545: 219.0,	Max: 2.0
VOG02551: 293.0,	Max: 2.0
VOG02552: 217.0,	Max: 2.0
VOG02558: 226.0,	Max: 1.0
VOG02559: 391.0,	Max: 5.0
VOG02568: 609.0,	Max: 5.0
VOG02571: 137.0,	Max: 2.0
VOG02572: 254.0,	Max: 2.0
VOG02592: 245.0,	Max: 2.0
VOG02598: 147.0,	Max: 1.0
VOG02602: 397.0,	Max: 3.0
VOG02606: 129.0,	Max: 2.0
VOG02617: 114.0,	Max: 2.0
VOG02655: 167.0,	Max: 2.0
VOG02686: 770.0,	Max: 2.0
VOG02694: 123.0,	Max: 2.0
VOG02700: 108.0,	Max: 2.0
VOG02729: 157.0,	Max: 1.0
VOG02739: 125.0,	Max: 1.0
VOG02821: 234.0,	Max: 1.0
VOG02829: 113.0,	Max: 1.0
VOG02835: 111.0,	Max: 1.0
VOG02837: 469.0,	Max: 2.0
VOG02894: 179.0,	Max: 1.0
VOG02902: 102.0,	Max: 2.0
VOG02936: 348.0,	Max: 2.0
VOG02947: 212.0,	Max: 2.0
VOG02960: 120.0,	Max: 1.0
VOG02967: 117.0,	Max: 1.0
VOG02991: 103.0,	Max: 2.0
VOG03017: 116.0,	Max: 1.0
VOG03047: 120.0,	Max: 2.0
VOG03054: 124.0,	Max: 1.0
VOG03058: 119.0,	Max: 2.0
VOG03145: 124.0,	Max: 1.0
VOG03151: 216.0,	Max: 3.0
VOG03155: 117.0,	Max: 2.0
VOG03190: 102.0,	Max: 1.0
VOG03234: 113.0,	Max: 2.0
VOG03239: 115.0,	Max: 1.0
VOG03252: 114.0,	Max: 1.0
VOG03272: 115.0,	Max: 2.0
VOG03282: 115.0,	Max: 1.0
VOG03305: 124.0,	Max: 2.0
VOG03332: 123.0,	Max: 2.0
VOG03334: 135.0,	Max: 2.0
VOG03439: 111.0,	Max: 1.0
VOG03490: 119.0,	Max: 1.0
VOG03499: 111.0,	Max: 2.0
VOG03513: 276.0,	Max: 3.0
VOG03592: 123.0,	Max: 2.0
VOG03643: 197.0,	Max: 2.0
VOG03712: 113.0,	Max: 2.0
VOG03715: 116.0,	Max: 1.0
VOG04004: 133.0,	Max: 1.0
VOG04243: 127.0,	Max: 1.0
VOG04596: 103.0,	Max: 1.0
VOG04730: 129.0,	Max: 2.0
VOG09451: 122.0,	Max: 2.0
VOG09452: 113.0,	Max: 1.0
VOG09455: 219.0,	Max: 2.0
VOG09471: 133.0,	Max: 3.0
VOG09476: 198.0,	Max: 1.0
VOG09480: 233.0,	Max: 1.0
VOG09481: 235.0,	Max: 2.0
VOG09484: 117.0,	Max: 1.0
VOG09485: 245.0,	Max: 1.0
VOG09486: 101.0,	Max: 1.0
VOG09497: 108.0,	Max: 1.0
VOG09499: 108.0,	Max: 2.0
VOG09500: 210.0,	Max: 2.0
VOG09502: 141.0,	Max: 2.0
VOG09511: 101.0,	Max: 2.0
VOG09512: 118.0,	Max: 1.0
VOG09513: 118.0,	Max: 1.0
VOG09514: 118.0,	Max: 1.0
VOG09515: 234.0,	Max: 1.0
VOG09516: 247.0,	Max: 3.0
VOG09517: 734.0,	Max: 2.0
VOG09520: 111.0,	Max: 1.0
VOG09521: 113.0,	Max: 2.0
VOG09522: 229.0,	Max: 1.0
VOG09536: 222.0,	Max: 1.0
VOG09537: 112.0,	Max: 2.0
VOG09544: 110.0,	Max: 1.0
VOG09558: 179.0,	Max: 1.0
VOG09580: 150.0,	Max: 1.0
VOG09620: 171.0,	Max: 3.0
VOG09725: 110.0,	Max: 1.0
VOG09730: 102.0,	Max: 1.0
VOG09731: 101.0,	Max: 1.0
VOG09732: 118.0,	Max: 1.0
VOG09733: 114.0,	Max: 2.0
VOG09734: 117.0,	Max: 2.0
VOG09738: 112.0,	Max: 2.0
VOG09743: 111.0,	Max: 1.0
VOG09744: 109.0,	Max: 2.0
VOG09748: 102.0,	Max: 1.0
VOG09749: 114.0,	Max: 1.0
VOG09752: 110.0,	Max: 1.0
VOG09753: 112.0,	Max: 1.0
VOG09754: 102.0,	Max: 2.0
VOG09755: 118.0,	Max: 2.0
VOG09835: 121.0,	Max: 1.0
VOG09842: 103.0,	Max: 1.0
VOG10017: 178.0,	Max: 3.0
VOG10087: 101.0,	Max: 1.0
VOG10223: 196.0,	Max: 2.0
VOG10810: 144.0,	Max: 1.0
VOG10811: 253.0,	Max: 2.0
VOG10812: 373.0,	Max: 2.0
VOG10813: 180.0,	Max: 1.0
VOG10814: 149.0,	Max: 2.0
VOG10815: 148.0,	Max: 1.0
VOG10890: 116.0,	Max: 1.0
VOG10923: 134.0,	Max: 1.0
VOG11077: 112.0,	Max: 2.0
VOG11130: 118.0,	Max: 1.0
VOG11293: 106.0,	Max: 1.0
VOG11392: 116.0,	Max: 3.0
VOG11421: 110.0,	Max: 2.0
VOG12842: 114.0,	Max: 1.0
VOG13250: 103.0,	Max: 2.0
VOG13834: 101.0,	Max: 1.0
VOG13846: 103.0,	Max: 1.0
VOG19701: 136.0,	Max: 2.0
VOG23961: 129.0,	Max: 1.0
There are 415 VOGS that appear more than 100 times
