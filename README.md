# CS3824 Giant Viruses

Trimming of the .tsv
    
I picked 100 as an arbitrary cut off for a VOG that occurs frequently enough. Then, I scanned through the original matrix and counted all of the occurences of a particular VOG across all the genomes. I reported both the total occurences and the maximum occurence in a single genome for all the 'good enough' VOG's. Using the offsets of these VOG's, I generated a new .tsv file. Only about 2% of the VOG's from the original .tsv were 'good enough.' Also, it was interesting to note that, while the maximum occurence of a VOG in a single genome was 18, the majority of VOG's occured at most 2 or 3 times in a single genome.

Subsets of Genomes

Using the maximum occurences of 'good enough' VOG's as a starting point, I compiled a list of genomes that contain
at least 5 copies of a single VOG. There is only a handful of VOG's that contribute to this list so this will be a nice toy subset of genomes to use as an input in preliminary tests. No genomes were listed more than once in this list.

Use of Bayesian Nonparametric clustering for Python (bnpy)

This library allows Python Programmers to apply state-of-the-art clustering models to large data sets and to perform unsupervised machine learning using Dirichlet process models. The desired inputs are comma seperated value files in which each row represents a different observation. Therefore, we have transposed our tsv files and converted them to csv files. In addition, we must test to the limitations of bnpy with various input sizes. To accomplish this, we used 'big_max.tsv,' which includes only eight VOG's, to generate test files with fewer genomes. The small version with 33 genomes inlcudes only genomes that contain an occurence of at least three of the VOG's. The medium version with 282 genomes seeks at least two of the eight VOG's and the large version with 986 genomes seeks at least one of the eight VOG's.
